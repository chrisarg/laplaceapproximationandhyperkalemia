//#define CPPAD_MAX_NUM_THREADS 100  // set this to the number of threads you want (default is 48)
#include <TMB.hpp>
// POISSON regression family for the estimation of relative risk of hyperkalemia
// uses random effects model (random intercept) to model interindividual variation
// and facility level variation


template<class Type>
Type objective_function<Type>::operator() ()
{
	DATA_VECTOR(y);			// event counts (0 or 1 for this analysis)
	DATA_MATRIX(X);			// fixed effects regression
	DATA_FACTOR(u_X);		// cluster membership for each observation 
	DATA_VECTOR(off_m);		// offset for the mean (length of stay for this application)
	PARAMETER_VECTOR(b);		// fixed regression effects
	PARAMETER_VECTOR(u_m);	// random intercept for single RE
	PARAMETER(sigmu);		// standard deviation of the random effects (u_m) 


	// negative log-likelihood
	Type res = Type(0.0);					// comment out this line and uncomment the next for ...
	// parallel_accumulator<Type> res(this);	// ... parallel likelihood evaluation

	vector<Type> eta(y.size());			// number of observations
	long int nclust = u_m.size();		// number of individuals
	eta = X*b;

	// construction of negative log-likelihood
	for (long int i = 0; i<y.size(); i++){
		res -= dpois(y[i], exp(u_m[u_X[i] - 1] + off_m[i] + eta[i]), true);
	}
	// random effect contribution to the negative log-likelihood
	for (long int i = 0; i<nclust; i++){
		res -= dnorm(u_m[i], Type(0.0), sigmu, true);
	}

	return res;
}
