# README #

To run the programs found here, you will need a working R installation,
preferably Microsoft R v3.5.3 and higher.

### What is this repository for? ###

* Simulates and constrasts various approaches to mixed model estimation
* from Adaptive Gaussian Hermite (AGH) to the interconnected Generalized
* Linear Model algorith used to fit Generalized Linear Mixed Models with
* the h-likelihood, against a direct, high performance implementation of
* latter using the R package TMB.
* The software is intended to illustrate the implementation presented in
* https://arxiv.org/abs/1910.08179
* Version 1.0


### How do I get set up? ###

* Dependencies: Rtools and the packages TMB, glmmTMB, rstanarm, lme4, ggplot2
* If you would like to compile the dynamically linked libraries for your
* platform, run the CompileHlikTemplates.R file first. The template (.CPP)
* files contain the C++ files show the code used to implement the Poisson
* GLMM. There are 4 templates: for single and multiple random effect models,
* and for parallel and serial codes.
* To generate a synthetic dataset, open the GenerateSyntheticDataset.R file
* and explore relative risk ratios, size of the dataset, number of random
* effects etc. The file generates a dataset sims.RData
* There are two folders in the root directory:
*    SingleRE: analyzes a Poisson GLM with a single random effect
*  MultipleRE: analyzes a Poisson GLM with multiple random effects


### Who do I talk to? ###

* Corresponding author listed in the arxiv submission
